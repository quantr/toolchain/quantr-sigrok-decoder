import sigrokdecode as srd
from functools import reduce


class ChannelError(Exception):
	pass


class Decoder(srd.Decoder):
	api_version = 3
	id = 'quantr'
	name = 'Quantr'
	longname = 'Quantr protocol'
	desc = 'Protocol used by Quantr receiver.'
	license = 'gplv2+'
	inputs = ['logic']
	outputs = []
	tags = ['Embedded/industrial']
	options = (
		{'id': 'freq', 'desc': 'Frequency', 'default': '100k', 'values': (
			'1k', '2k', '4k', '5k', '10k', '20k', '40k', '50k', '100k')},
		{'id': 'skipbit', 'desc': 'Skip bit', 'default': 0},
	)
	channels = (
		{'id': 'quantr', 'name': 'Quantr', 'desc': 'Quantr data line'},
	)
	annotations = (
		('bin', 'bin'),
		('hex', 'hex'),
		('string', 'string'),
	)
	annotation_rows = (
		('bins', 'bins', (0,)),
		('hexs', 'hexs', (1,)),
		('strings', 'strings', (2,)),
	)

	def __init__(self):
		self.reset()

	def reset(self):
		self.samplerate = None
		self.offset = 7
		self.byte = 0
		self.index = 0
		self.realIndex = 0
		self.shouldStart = False
		self.beginSampleNum = 0

	def metadata(self, key, value):
		if key == srd.SRD_CONF_SAMPLERATE:
			self.samplerate = value

	def start(self):
		self.out_ann = self.register(srd.OUTPUT_ANN)

	def decode(self):
		print("quantr decode, samplenum=", self.samplenum)
		self.offset = 7
		if self.options['freq'] == "1k":
			freq = 1*1000
		elif self.options['freq'] == "2k":
			freq = 2*1000
		elif self.options['freq'] == "4k":
			freq = 4*1000
		elif self.options['freq'] == "5k":
			freq = 5*1000
		elif self.options['freq'] == "10k":
			freq = 10*1000
		elif self.options['freq'] == "20k":
			freq = 20*1000
		elif self.options['freq'] == "40k":
			freq = 40*1000
		elif self.options['freq'] == "50k":
			freq = 50*1000
		elif self.options['freq'] == "100k":
			freq = 100*1000
		else:
			freq = 10*1000

		if not self.samplerate:
			raise ChannelError('Cannot decode without samplerate.')
		while True:
			#print("quantr decode, samplenum=", self.samplenum,", offset=",self.offset)
			(quantr,) = self.wait({'skip': 0})
			if self.samplenum == self.options['skipbit']:
				self.shouldStart = True

			if self.shouldStart:
				self.put(self.index, self.index+1,
							self.out_ann, [0, ['%x' % quantr]])
				self.byte += (quantr << self.offset)
				self.offset -= 1

			if self.shouldStart and (self.index+1) % 8 == 0:
				self.put(self.index, self.index+8,
							self.out_ann, [1, ['%x' % self.byte]])
				self.put(self.index, self.index+8,
							self.out_ann, [2, ['%c' % self.byte]])
				self.offset = 7
				self.byte = 0

			if self.shouldStart:
				self.index += 1

			(quantr,) = self.wait({'skip': 1})
