# Introduction

This is an example code to create a dummy decoder for pulseview

# Steps to use it

1. Install pulseview https://sigrok.org/wiki/Downloads

2. Open pulseview, find out the path that stores all decoders

![](https://www.quantr.foundation/wp-content/uploads/2022/02/sigrok1.png)

3. Git clone this repo into that folder

![](https://www.quantr.foundation/wp-content/uploads/2022/02/sigrok2.png)

4. Restart pulseview, then you will see your decode is on screen

![](https://www.quantr.foundation/wp-content/uploads/2022/02/sigrok4.png)

5. If your code has problem, the way to decode it is to start pulseview by command line, the python error message will in the console

![](https://www.quantr.foundation/wp-content/uploads/2022/02/sigrok4.png)

